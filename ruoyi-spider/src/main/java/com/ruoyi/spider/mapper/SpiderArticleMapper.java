package com.ruoyi.spider.mapper;

import java.util.Map;

/**
 * 爬虫配置Mapper接口
 * 
 * @author wujiyue
 * @date 2019-11-11
 */
public interface SpiderArticleMapper
{
    public int insertArticle(Map map);

    public void insertArticleContent(Map map);

}

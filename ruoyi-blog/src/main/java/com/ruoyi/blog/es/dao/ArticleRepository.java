package com.ruoyi.blog.es.dao;

import com.ruoyi.blog.es.pojo.ArticleEntity;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("blogArticleRepository")
public interface ArticleRepository extends ElasticsearchRepository<ArticleEntity, String> {
    public ArticleEntity findArticleEntitiesById(String id);

    public int deleteArticleEntitiesById(String id);

    public List<ArticleEntity> findByTitleLikeOrContentLike(String title, String content);
}
